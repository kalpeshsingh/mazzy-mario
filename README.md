### Welcome to Mazzy Mario

Demo - [MazzyMario](https://knowkalpesh.in/mazzyMario/)

#### How to play game
- Use either on screen arrow key controls or keyboard arrows
- You goal is to reach the flower
- You will get alert message once you reach your goal
- You can reset your game any time or change type of maze