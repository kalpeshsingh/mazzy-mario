import React, { Component } from 'react';
class Control extends Component {
    render() {
        // Destrucutre and attach click event listener
        // 37 = left, 38 = up, 39 = right, 40 = down
        const { change } = this.props;
        return (
            <div className="arrow-key-container">
                <div className="arrow-key arrow--up" onClick={() => {change(38)}}></div> <br />
                <div className="arrow-key arrow--left" onClick={() => {change(37)}}></div>
                <div className="arrow-key arrow--down" onClick={() => {change(40)}}></div>
                <div className="arrow-key arrow--right" onClick={() => {change(39)}}></div>
          </div>
        );
    }
}

export default Control;