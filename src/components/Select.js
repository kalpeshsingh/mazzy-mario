import React, { Component } from 'react';
class Select extends Component {
    // Populate select option given by props and return select box
    render() {
        const { options, change } = this.props;
        return (
            <select onChange={change}>
                {
                    options.map((option, idx) => {
                        return(<option key={idx} value={idx}>{option.name}</option>);
                    })
                }
            </select>
        );
    }
}

export default Select;