import React, { Component } from 'react';
import Alert from './Alert';
import Button from './Button';
import Select from './Select';
import { data } from './../data';
import Control from './Control';
class Maze extends Component {

  // Player initial position
  state = {
    player: {
      x: this.props.data.start[0],
      y: this.props.data.start[1]
    }
  }


  // Create ref. for canvas element
  canvas = React.createRef();

  drawMaze = (width, height, data) => {

    // Get map value
    const map = data.map;

    //Set canvas context
    const ctx = this.refs.canvas.getContext('2d');

    // Set game icons
    const playerIcon = new Image();
    playerIcon.src = './icons/player.png';

    const goalIcon = new Image();
    goalIcon.src = './icons/goal.png';

    const { x: moveX, y: moveY } = this.state.player;
    ctx.clearRect(0, 0, width, height);
    
    // Set block size
    const blockSize = parseInt(width / map.length, 10);

    // Put -1 to goal
    const [endX, endY] = data.end;
    map[endY][endX] = -1;

    for (let y = 0; y < map.length; y++) {
      for (let x = 0; x < map[y].length; x++) {
        //Draw a wall
        if (map[y][x] === 0) {

          (function (_x, _y) {
            const wallIcon = new Image();
            wallIcon.onload = function () {
              ctx.drawImage(wallIcon, _x * blockSize, _y * blockSize, blockSize, blockSize);
            };
            wallIcon.src = './icons/wall.png';
          })(x, y);

        } else if (map[y][x] === -1) {
          // draw goal icon
          ctx.beginPath();
          goalIcon.onload = () => {
            ctx.drawImage(goalIcon, x * blockSize, y * blockSize, blockSize, blockSize);
          }
        }
      }
    }

    // else draw path
    ctx.beginPath();
    playerIcon.onload = () => {
      ctx.drawImage(playerIcon, moveX * blockSize, moveY * blockSize, blockSize, blockSize);
    }
  }

  // A pure function that checks valid move and return boolean value
  isValidMove = (x, y) => {
    return (y >= 0) && (y < this.props.data.map.length) && (x >= 0) && (x < this.props.data.map[y].length) && (this.props.data.map[y][x] !== 0);
  }

  // Render an alert if player reached its goal
  renderAlert = () => {
    const { x, y } = this.state.player;
    const { data } = this.props;
    const isGoal = ((data.end[0] === x) && data.end[1] === y);
    if (isGoal) {
      return <Alert cls={`alert--success`} text="Congratulations! You reached your goal. 😎" />
    }
  }

  // Put player in initial position and draw the maze again
  handleReset = () => {
    const { player } = this.state;
    const { width, height, data } = this.props;
    const [startX, startY] = this.props.data.start;
    player.x = startX;
    player.y = startY;
    this.setState({
      player
    });
    this.drawMaze(width, height, data);
  }

  // Update maze based on dropdown selection
  handleChange = (e) => {
    const { width, height, changeMaze } = this.props;
    const value = +e.currentTarget.value;
    this.setState({
      player: {
        x: data[value].start[0],
        y: data[value].start[1]
      }
    }, () => {
          // Update State in the App.js
          // TODO: Such kind of methods should be in App.js itself. Need to re-arrange this part.
          changeMaze(value);
          this.drawMaze(width, height, data[value]);
    });

  }

  // Move the player in specific direction
  // 37 = left, 38 = up, 39 = right, 40 = down
  // TODO: Convert it into switch case
  updatePlayerMove = (key) => {

    const { width, height, data } = this.props;
    const { player } = this.state;
    if ((key === 38) && this.isValidMove(player.x, player.y - 1)) {
      player.y--;
      this.setState({
        player
      });
    } else if ((key === 40) && this.isValidMove(player.x, player.y + 1)) {
      player.y++;
      this.setState({
        player
      });
    } else if ((key === 37) && this.isValidMove(player.x - 1, player.y)) {
      player.x--;
      this.setState({
        player
      });
    } else if ((key === 39) && this.isValidMove(player.x + 1, player.y)) {
      player.x++;
      this.setState({
        player
      });
    }
    this.drawMaze(width, height, data);
  }

  componentDidMount() {
    // Call drawMaze method on keyup and when page loads
    const { width, height, data } = this.props;
    const that = this;

    window.addEventListener('keydown', function (e) {
      e.preventDefault();
      that.updatePlayerMove(e.which);
    });
    this.drawMaze(width, height, data);
  }

  render() {
    // Destrucutre canvas width, height
    const { width, height } = this.props;
    return (
      <div className="canvas-container" style={{ width }}>
        <canvas ref="canvas" className="maze" width={width} height={height} />
        {this.renderAlert()}
        <div className="controls">
          <Button cls={`button--danger`} text="Reset game" click={this.handleReset} />
          <Select options={this.props.mazeOptions} change={this.handleChange} />
        </div>
        <Control change={this.updatePlayerMove} />
      </div>
    );
  }
}

export default Maze;