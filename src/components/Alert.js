import React, { Component } from 'react';
class Alert extends Component {
    render() {
      // Destrucutre class names and text to display
        const { cls, text } = this.props;
        return (
          <div className={`alert ${cls}`}>
            {text}
          </div>
        );
      }
}


export default Alert;