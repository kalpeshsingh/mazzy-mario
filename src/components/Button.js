import React, { Component } from 'react';
class Button extends Component {
    render() {
      // Destrucutre class names, text to display and attach click event listener
        const { cls, text, click } = this.props; 
        return (
          <div className={`button ${cls}`} onClick={click} >
            {text}
          </div>
        );
      }
}


export default Button;