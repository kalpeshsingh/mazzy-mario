import React, { Component } from 'react';
import './App.css';
import Maze from './components/Maze';
import { data } from './data';

class App extends Component {

  // Maze dimensions and maze type
  state = {
    mazeWidth: 400,
    mazeHeight: 400,
    currentType: 0
  }

  // Generate options for maze type
  mazeOptions = data.map((item, idx) => {
    return {
      name: item.name,
      value: idx
    }
  });

  // Handle maze option change
  handleChange = (value) => {
    this.setState({
      currentType: value
    })
  }

  render() {
    const { mazeWidth: width, mazeHeight: height } = this.state;
    return (
      <div className="container">
        <Maze width={width} height={height} data={data[this.state.currentType]} mazeOptions={this.mazeOptions} changeMaze={this.handleChange} />
        <div className="disclaimer"> 
          Images are for demo purpose only. Credit: <a href="https://www.iconfinder.com/sandro.pereira" target="_blank" rel="noopener noreferrer">Sandro Pereira, Iconfinder</a>
        </div>
      </div>
    );
  }
}

export default App;
